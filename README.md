# 授权 / LICENSE

本仓库采用 [WTFPL](https://gitlab.com/LiuliPack/userscript/-/blob/main/LICENSE) 授权。  
This repository is Licensed under [WTFPL](https://gitlab.com/LiuliPack/userscript/-/blob/main/LICENSE).


# 清单 / List

项目名(Title) | 描述(Desc) | 源码(Source)
-- | - | -
番茄时钟(Pomodoro) | 可以自定义的番茄时钟。 | [Web](Projects/AA00002/Pomodoro.html)
命运生成器(Fate Generator) | 它类似于[極彩花夢 Telegram 群](https://t.me/KyokuSai)的`/unmei`命令。生成随机内容。 | [Web](Projects/AA00001/FateGenerator.html) [Python](Projects/AA00001/FateGenerator.py)

## 网页收容 / WebContainment

来源(Origin) | 描述(Desc) | 源码(Source)
-- | - | -
https[:]//xingchengka[.]com | 行程卡纪念版停运说明。 |  [点/Click](Archive/A00002)
https[:]//www[.]zheteng[.]co/public/check.htm | 来自 [BV1sD4y187Kp](https://b23.tv/BV1sD4y187Kp) 的安全性测试。它涉及到剪切板的使用，如果有重要信息，记得先备份。 | [点/Click](Archive/A00001/check.html)

# 附录 / Appendix

## 约定式提交 1.0.1-本地化 / conventional commits 1.0.1-localization

本仓库基于[约定式提交 1.0.0](https://www.conventionalcommits.org/zh-hans/v1.0.0/约定式提交规范)的本地化版本。

```
[提交类型](涉及类型-涉及代码): (更新描述)

[必填] (可选)
```

提交类型 | 描述
-- | -
`新` | 新增项目、功能或收容更多页面。
`修` | 修正错误。
`优` | 优化代码。
`重` | 仅重构代码，不修复错误和新增功能。
`文` | 调整文档。
`整` | 调整文件位置。
`撤` | 撤销提交。