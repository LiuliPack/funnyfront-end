##!/usr/bin/env python3
# -*- coding: UTF-8 -*-

# 引入系统交互(OS)和随机数(random)库，定义语料(corpus)变量，定义随机内容(rand(data))和生成数据(gen())函数
import os, random

corpus = {
    "ind": ["祈祷失败"],
    "when": ["今天", "今天内", "明天", "今年内", "迟早"],
    "where": ["屋外", "电车"],
    "who": [" OL ", "魔物娘", "伪娘", "大小姐", "天使", "恶魔", "女医生", "学生", "少女", "机械", "萝莉", "女警", "青梅竹马", "魅魔", "魔法少女", "兔女郎", "女仆", "护士", "触手", "群友"],
    "whoAdj": ["穿丝袜", "穿体操服", "穿制服", "穿和服", "穿哥特萝莉", "穿女装", "穿拘束服", "穿泳衣", "穿紧身衣", "穿紧身裤", "穿胶衣", "严肃", "傲娇", "幻想", "附身", "恶堕", "被洗脑", "淫乱", "丰满", "兽耳", "扎双马尾", "处女", "巨乳", "短发", "稍胖", "苗条", "黝黑", "贫乳", "金发", "平胸", "扎马尾辫", "黑发"],
    "what": ["连续高潮", "色诱", "乳交", "盗摄", "催眠", "中出", "凌辱", "口交", "吞精", "外射", "奴隶", "强奸", "手交", "扩张", "拘束", "放置PLAY", "深喉", "灌肠", "瘙痒", "监禁", "紧缚", "肛交", "调教", "足交", "颜射", "性交"],
    "whatItem": ["润滑液", "玩具", "药物", "鞭子", "绳子", "蜡烛", "项圈", "锁链", "丝袜", "内衣", "内裤", "女装", "拘束服", "紧身衣", "紧身裤", "胶衣", "触手"],
    "punct": ["。", "！", "……"]
}

def rand(data):
    # 返回随机生成内容
    return data[random.randint(0, len(data) - 1)]

def gen():
    # 定义随机句式(Sentences)、随机人物形容词(Adj)、事件物品(Item)和地点(Where)变量
    Sentences = random.randint(0, 2)
    Adj = random.randint(0, 1)
    Item = random.randint(0, 1)
    Where = random.randint(0, 1)

    # 生成人物形容、事件物品和地点
    if Adj:
        Adj = rand(corpus["whoAdj"]) + "得"
    else:
        Adj = ''
    if Item:
        Item = "用" + rand(corpus["whatItem"])
    else:
        Item = ''
    if Where:
        Where = "在" + rand(corpus["where"])
    else:
        Where = ''

    # 生成命运
    if(Sentences == 0):
        # 句式一：您[时间]会[地点][事件物品][事件][人物属性][人物][句尾标点]
        return f'\n您{rand(corpus["when"])}会{Where}{Item}{rand(corpus["what"])}{Adj}{rand(corpus["who"])}{rand(corpus["punct"])}\n'
    if(Sentences == 1):
        # 句式二：您[时间]会被[人物属性][人物][地点][事件物品][事件][句尾标点]
        return f'\n您{rand(corpus["when"])}会被{Adj}{rand(corpus["who"])}{Where}{Item}{rand(corpus["what"])}{rand(corpus["punct"])}\n'
    if(Sentences == 2):
        # 句式三：[单句][句尾标点]
        return f'\n{rand(corpus["ind"])}{rand(corpus["punct"])}\n'

print(gen())

os.system('pause')